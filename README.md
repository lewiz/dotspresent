# dotspresent

dotspresent automatically installs dotfiles to remote SSH hosts if they are not
present or are not fresh.  Freshness is determined with a simple integer counter
stored in a file called [`.dotspresent`](.dotspresent).

## Installation

```bash
git clone https://bitbucket.org/lewiz/dotspresent.git ~/.dotspresent
echo "source ${HOME}/.dotspresent/dotspresent.sh" >> ~/.zshrc
```

## Configuration

A default set of configuration files for zsh, bash and vim are present within
this repo:

* [`.zshrc`](.zshrc)
* [`.vimrc`](.vimrc)
* [`.bashrc`](.bashrc)

These and any other files beginning with `.` present within `~/.dotspresent` are
copied to `$HOME` on machines accessed via SSH.

_Whenever you add new files or make changes you must update version in
[`dotspresent.sh`](dotspresent.sh) to allow changes to be detected and
synchronised._

## Alternatives

* Use SSH's `LocalCommand` option, as discussed at
  http://klaig.blogspot.co.uk/2013/04/make-your-dotfiles-follow-you.html.  This
  method requires just two SSH connections but assumes files should always be
  copied

## Future

* Combination of SSH `LocalCommand` and this existing SSH wrapper function to
  reduce install/upgrade case to just two SSH connections (down from three)
* Support bash on the local host
* Replace zsh then bash shell launching on remote host with a configurable set
    of shells
