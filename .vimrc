runtime! plugin/sensible.vim

set hls
"set mouse=a             " enable mouse in all modes
"set mousefocus
set tildeop             " make ~ swap case instead of g~
set previewheight=20    " quickfix window
set ignorecase
set smartcase
set shiftwidth=4        " default to 4-space indents
set expandtab           " never insert tab characters
set splitright          " new splits on the right (default: left)
set splitbelow          " new splits below (default: above)
if exists("+folding")
  set nofoldenable        " all folds open by default
endif
set updatetime=1000     " useful for tagbar
nnoremap ' `
nnoremap ` '
" space to jump between ,
nnoremap <space> f,<space>
vnoremap <space> f,<space>

"set encoding=utf-8

" Line numbering
if exists("+relativenumber")
  set rnu
else
  set nu
endif
set scrolloff=2

" Jump to last position when reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif

" Make window movement available without ^W first
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l
