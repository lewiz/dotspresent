. ${HOME}/.zsh/completion.zsh
. ${HOME}/.zsh/history.zsh
set -o vi
bindkey "^R" history-incremental-search-backward
PROMPT="%F{magenta}%n%F{green}@%F{yellow}%m %B%~%b %(?:%F{green}%%%f:%F{red}%%%f) "
