function ssh {
  LOCAL_VERSION="$(<${HOME}/.dotspresent/.dotspresent)"
  NEED_SEND_DOTFILES="[ ! -f \${HOME}/.dotspresent ] && exit 88; [ \$(<\${HOME}/.dotspresent) -lt ${LOCAL_VERSION} ] && exit 89"
  ZSH_OR_BASH="[ -x /bin/zsh ] && exec /bin/zsh; exec /bin/bash"
  command ssh "$@" -t "${NEED_SEND_DOTFILES}; ${ZSH_OR_BASH}"
  ret=$?
  if [[ ${ret} -eq 88 || ${ret} -eq 89 ]]; then
    [ ${ret} -eq 88 ] && echo "Installing dotfiles and reconnecting..."
    [ ${ret} -eq 89 ] && echo "Upgrading dotfiles and reconnecting..."
    tar -zc -C${HOME}/.dotspresent $(cd ~/.dotspresent; for file in .*; do echo $file; done) | command ssh "$@" "tar -zx -C\${HOME}" && \
      command ssh "$@" -t "${ZSH_OR_BASH}"
  fi
}
